void prohod( int* a, int *b )
{
    int c;
    c = *a;
    *a = *b;
    *b = c;
}

void trideniVyberem( int* data, int N )
{
    for(int i = 0; i < N-1; i++)
    {
        int nejmensi = i;
        for(int j = i+1; j < N; j++)
        {
            if(data[nejmensi] > data[j])
                nejmensi = j;
        }
        prohod( &data[ i ], &data[ nejmensi ] );
    }
}

void trideniVkladanim( int* data, int N )
{
    for(int i = 1; i < N; i++)
    {
        int j = i;
        int hodnota = data[i];
        while(j > 0 && data[j-1] > hodnota){
            data[j]=data[j-1];
            j--;
        }
        data[j]=hodnota;
    }
}


void bublinkoveTrideni( int* data, int N )
{
    for (int i = 0; i < N - 1; i++)
    {
        for (int j = 0; j < N - i - 1; j++ )
        if (data[j]>data[j+1])
            prohod(&data[j], &data[j+1]);
    }
}

void bublinkoveTrideni2( int* data, int N )
{
    int l( 0 ), r( N );
    while( l < r )
    {
        int posledniZmena;
        for (int j = l; j < r - 1; j++ )
            if (data[j]>data[j+1])
            {
                prohod(&data[j], &data[j+1]);
                posledniZmena = j;
            }
        r = posledniZmena;
        for (int j = r - 1; j >= l; j-- )
            if (data[j]>data[j+1])
            {
                prohod(&data[j], &data[j+1]);
                posledniZmena = j;
            }
        l = posledniZmena + 1;
    }
}

void quickSort( int* data, int posledni, int prvni = 0 )
{
    if( posledni - prvni < 5 )
    {
        trideniVkladanim( &data[ prvni], posledni - prvni );
        return;
    }    
    
    int pivot = data[posledni-1];   
    int r = posledni-2;
    int l = prvni;
    
    while (l<r )        
    {              
        while (data[l]<pivot)   
            l++;
        while (data[r]>pivot && r > l)   
            r--;        
       if (l<r)
           prohod(&data[l], &data[r]);                   
    }
    
    if (data[l] > pivot) 
    {
        prohod(&data[l], &data[posledni-1]);
        quickSort(data, l, prvni);
        quickSort(data, posledni, l+1);
    }
    else 
    {
        prohod(&data[l+1], &data[posledni-1]);
        quickSort(data, l+1, prvni);
        quickSort(data, posledni, l+2);
    }
}









