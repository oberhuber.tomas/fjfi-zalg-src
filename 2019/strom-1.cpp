#include "pch.h"
#include <iostream>

struct Uzel
{
	Uzel(double d);

	double data;
	Uzel *levy, *pravy;
};

Uzel::Uzel(double d)
{
	data = d;
	levy = nullptr;
	pravy = nullptr;
}

struct BinarniStrom
{
	BinarniStrom();

	void vloz(double data);

	bool najdi(double data);

	~BinarniStrom();

	void vlozDoPodstromu(Uzel* podstrom, double data);

	void smazPodstrom(Uzel* podstrom);

	bool najdiVPodstromu(Uzel* podstrom, double data);

	Uzel* koren;
};

BinarniStrom::BinarniStrom()
{
	koren = nullptr;
}

void BinarniStrom::vloz(double data)
{
	if (koren == nullptr)
		koren = new Uzel( data );
	else
		vlozDoPodstromu(koren, data);
}

bool BinarniStrom::najdi(double data)
{
	return najdiVPodstromu(koren);
}

void BinarniStrom::vlozDoPodstromu(Uzel* koren, double data)
{
	if (data < koren->data)
	{
		if (koren->levy != nullptr)
			vlozDoPodstromu(koren->levy,data);
		else
			koren->levy = new Uzel( data );
	}
	if (data > koren->data)
	{
		if (koren->pravy != nullptr)
			vlozDoPodstromu(koren->pravy, data);
		else
			koren->pravy = new Uzel(data);
	}
}

bool BinarniStrom::najdiVPodstromu(Uzel* podstrom, double data)
{
	if (podstrom == nullptr)
		return false;
	if (data == podstrom->data)
		return true;
	if (data < podstrom->data)
		return najdiVPodstromu(podstrom->levy, data);
	else
		return najdiVPodstromu(podstrom->pravy, data);
}


BinarniStrom::~BinarniStrom()
{
	smazPodstrom(koren);
}

void BinarniStrom::smazPodstrom(Uzel* podstrom)
{
	if (podstrom->levy != nullptr)
		smazPodstrom(podstrom->levy);
	if (podstrom->pravy != nullptr)
		smazPodstrom(podstrom->pravy);
	delete podstrom;
}

int main()
{
    std::cout << "Hello World!\n"; 

	BinarniStrom strom;

	strom.vloz(5);

}

