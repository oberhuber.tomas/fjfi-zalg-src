#include <cstdlib>
#include <iostream>
#include "BinarniStrom.h"

using namespace std;


void test_najdi()
{
   BinarniStrom s;
   if( s.najdi( 0 ) )
      std::cerr << "Test: najdi        SELHAL: " << __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;
}

void test_vloz()
{
   BinarniStrom s;
   s.vloz( 9 );
   s.vloz( 5 );
   s.vloz( 11 );
   s.vloz( 2 );
   s.vloz( 3 );
   s.vloz( 6 );
   s.vloz( 7 );
   s.vloz( 2 );

   ////
   // Testujeme, zda jsou ve stromu vlozene hodnoty.
   if( ! s.najdi( 9 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   if( ! s.najdi( 5 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   if( ! s.najdi( 11 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   if( ! s.najdi( 2 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   if( ! s.najdi( 3 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   if( ! s.najdi( 6 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   if( ! s.najdi( 7 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   ////
   // Testujeme, zda ve stromu nejsou prvky, ktere jsme do nej nevlozili
   if( s.najdi( 0 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   if( s.najdi( 4 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   if( s.najdi( 8 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   if( s.najdi( 10 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

   if( s.najdi( 12 ) )
      std::cerr << "Test: najdi        SELHAL: " <<  __FILE__ << ":" << __LINE__ << std::endl;
   else
      std::cerr << "Test: najdi        OK" << std::endl;

}


int main( int argc, char** argv )
{
   test_najdi();
   test_vloz();
   return EXIT_SUCCESS;
}

