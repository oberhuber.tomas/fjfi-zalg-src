﻿#include <iostream>

////
// Velikost hry. Lze nastavit vetsi hodnoty jako 16, 25 apod.
const int N = 9;

/**
 * Tato funkce vrati souradnice dalsiho nezadaneho policka.
 * Musi tedy brat v potaz zadana policka a stejne tak si musi
 * poradit s prechodem na dalsi radek. Funkce vraci true, pokud 
 * existuje dalsi takove policko, jinak vraci false.
 */
bool dalsi( bool zadane[ N ][ N ], int *radek, int *sloupec)
{
}

/**
 * Tato funkce vrati souradnice predchoziho nezadaneho policka.
 * Musi tedy brat v potaz zadana policka a stejne tak si musi
 * poradit s prechodem na predchozi radek. Funkce vraci true, pokud 
 * existuje predchozi takove policko, jinak vraci false.

 */
bool predchozi( bool zadane[ N ][ N ], int *radek, int *sloupec)
{
}

/**
 * Tato funkce testuje, zda na pozici 'radek', 'sloupec' mohu zapsat cislo 
 * 'hodnota' abych pritom neporusil  pravidla sudoku.
 */
bool jePripustna( int sudoku[ N ][ N ], int radek, int sloupec, int hodnota)
{
}

/**
 * Tato funkce vypise reseni. Pokdu chcete, muzete nejakym zpusobem
 * oznacit i zadana policka.
 */
void vypis( int sudoku[ N ][ N ], bool zadane[ N ][ N ] )
{
}

bool resSudoku( int sudoku[ N ][ N ], bool zadane[ N ][ N ] )
{
   ////
   // Zde napiste kod pro reseni ulohy sudoku.
   // 1. Napiste funkci jePripustna, ktera bude overovat, ze cislo na 
   // pozici 'radek, 'sloupec' odpovida providlum Sudoku.
   // 2. Budeme prochazet postupne zleva doprava a sharo dolu jednotliva
   // nezadana policka sudoku a zkouset na ne zapsat cisla 1 az 9. Pomoci
   // funkce 'jePripustna' budeme zjistovat, zda tuto hodnotu lze zapsat nebo ne.
   // Pokud ano, hodnotu zapiseme a posuneme se na dalsi nezadane policko (to resi funkce 'dalsi').
   // Pokud ne, zkousime hodnotu o jedna vetsi. Pokud se nepodari zapsat ani devitku, vracime se na 
   // predchozi nezadane policko (to resi funkce 'predchozi') a sem zkousime zapsat opet o jedna vetsi hodnotu.
   // Dojdeme-li do stavu, ze mame zaplnena vsechna policka, vypiseme reseni.
}


int main()
{
   ////
   // Sudoku je reprezentovano pomoci 2D poli.
   // V tomto poli jsou hodnoty na jednotlivych polickach ulohy.
   int sudoku[ N ][ N ];

   ////
   // V tomto poli jsou hodnotou 'true' oznacene zadane hodnoty,
   // tj. ta policka, ktera nebudeme behem reseni menit.
   bool zadane[ N ][ N ];
   
   ////
   // Nejprve obe pole vynulujeme
   for( int i = 0; i < N; i++ )
      for (int j = 0; j < N; j++)
      {
         sudoku[ i ][ j ] = 0;
         zadane[ i ][ j ] = false;
      }

   ////
   // Zadame hodnoty nektere ulohy:
   sudoku[ 0 ][ 0 ] = 8;
   sudoku[ 1 ][ 2 ] = 3;
   sudoku[ 2 ][ 1 ] = 7;
   sudoku[ 2 ][ 4 ] = 9;
   sudoku[ 2 ][ 6 ] = 2;
   // atd. doplnte si sami
   
   ////
   // Nasledne oznacime zadana policka
   for( int i = 0; i < N; i++ )
      for( int j = 0; j < N; j++ )
         if( sudoku[ i ][ j ] != 0 ) zadane[ i ][ j ] = true;
	
   ////
   // Resime ulohu	
   resSudoku( sudoku, zadane );
}

