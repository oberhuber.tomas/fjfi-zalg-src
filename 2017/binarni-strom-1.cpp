#include "binarni-strom.h"

PrvekStromu::PrvekStromu( const int _data )
{
   data = _data;
   levy = NULL;
   pravy = NULL;
}

Strom::Strom()
{
   koren = NULL;
}

void Strom::VlozPrvek( const int data )
{
   if (koren==NULL)
   {
      koren = new PrvekStromu( data );
      return;
   }
   PrvekStromu *predchudce = NULL;
   PrvekStromu *soucasny = koren;
   if( NajdiVeStromu(data, &soucasny, &predchudce ) == true )
   {
      std::cout << "Prvek uz je ve stromu," << std::endl;
      return;
   }
   assert(predchudce != NULL);
   assert(predchudce->data != data);
   
   if(predchudce->data < data)
   {
      predchudce->pravy = new PrvekStromu( data );
   }
   else predchudce->levy = new PrvekStromu( data );
}


bool Strom::NajdiVeStromu( const int data,
                           PrvekStromu **soucasny,
                           PrvekStromu **predchudce)
{
   if (*soucasny==NULL)
   {
      return false;
   }
   if ( (*soucasny)->data == data)
   {
      return true;
   }
   if ((*soucasny)->data > data)
   {
      *predchudce = *soucasny;
      *soucasny = (*soucasny)->levy;
      NajdiVeStromu(data, soucasny, predchudce );         
   }
   else
   {
      *predchudce = *soucasny;
      *soucasny = (*soucasny)->pravy;
      NajdiVeStromu(data, soucasny, predchudce );
   }
}
   


