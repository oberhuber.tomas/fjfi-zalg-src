#ifndef BINARNI_STROM_H
#define BINARNI_STROM_H

struct PrvekStromu
{
   PrvekStromu( const int data );
      
   int data;
   
   PrvekStromu *levy, *pravy;
};

struct Strom
{
   Strom();
   
   void VlozPrvek( const int data );
   
   bool NajdiPrvek( const int data );
   
   void SmazPrvek( const int data );
   
   ~Strom();
   
   // Pomocne metody
   
   void NajdiVeStromu(const int data, PrvekStromu *podstrom,
   PrvekStromu **predchudce, PrvekStromu **prvek, PrvekStromu **nasledovnik);
   
   PrvekStromu* koren;
};


#endif /* BINARNI_STROM_H */

