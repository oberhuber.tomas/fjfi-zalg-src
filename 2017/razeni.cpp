void prohod( int* a, int* b )
{
   int k = *a;
   *a = *b;
   *b = k;
}

void primyVyber( int* a, int n )
{
   for( int i=0; i<n-1; i++)
      for(int j=i+1; j<n; j++)
         if(a[j]<a[i])
            prohod( &a[i], &a[j] );
}
   
void primeVkladani( int* a, int n )
{
   for (int i = 1; i < n; i++){
      int vkladanyPrvek = a[i];
      int j = i-1;
      while (j>=0 && vkladanyPrvek < a[j] ){
         a[j+1]=a[j];
         j--;
      }
      a[j+1] = vkladanyPrvek;
   }
}


void quicksort( int* a, int n )
{
   if( n < 2 )
      return;
   int  pivot= a[n-1]; 
   int leva = 0; 
   int prava=n-2;
   while( leva < prava )
   { 
      while(a[leva]<pivot) leva++; 
      while( prava > leva && a[prava]>pivot ) prava--; 
      if( a[ leva ] > a[ prava ] )
      {
         prohod(&a[leva],&a[prava]);
         leva++;
         prava--;
      }
   }
   if( a[leva] < pivot ) leva++;
   prohod( &a[leva],&a[n-1]);
   quicksort( a, leva );
   quicksort( &a[ leva + 1 ], n - leva - 1 );
}


















