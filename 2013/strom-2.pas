type ukVrchol = ^vrchol;

vrchol = record
  data : integer;
  levy, pravy : ukVrchol;
end;

procedure Vlozeni( data:integer;var strom:ukVrchol )

function Najdi( data:integer; strom:ukVrchol ) : boolean

procedure SmazStrom( strom:ukVrchol )

procedure Smaz( data:integer )

procedure Vlozeni( data:integer;var strom:ukVrchol )
  begin
    if strom=nil then
    begin
      strom:=new(Vrchol);
      strom^.data:=data;
      strom^.levy:=nil;
      strom^.pravy:=nil;
    end else
    begin
      if strom^.data<data then Vlozeni(data, strom^.pravy)
      else Vlozeni(data, strom^.levy)
    end;
  end;

function Najdi( data:integer; strom:ukVrchol ) : boolean
  begin
    if strom=nil then najdi:=false else 
    begin
      if strom^.data=data then najdi:=true
      else begin
        if strom^.data<data then najdi:=najdi(data, strom^.pravy)
        else najdi:=najdi(data, strom^.levy); 
      end;
    end;
  end;


procedure SmazStrom(var strom:ukVrchol )
begin
  if strom<>nil then
    begin
      smazstrom(strom^.levy);
      smazstrom(strom^.pravy);
    dispose (strom);
    strom:=nil;
    end; 
end;


function najdiVeStromu (data:integer; strom:ukVrchol; var Predchozi:ukVrchol):ukVrchol
   begin
     if strom=nil then najdiVeStromu:=nil else 
    begin
      if strom^.data=data then najdiVeStromu:=strom
      else begin
        Predchozi:=strom;
        if strom^.data<data then najdiVeStromu:=najdiVeStromu(data, strom^.pravy, Predchozi)
        else najdiVeStromu:=najdiVeStromu(data, strom^.levy, Predchozi); 
      end;
    end;
   end;

procedure SmazList (list,Predchozi:ukVrchol)
  begin
    if Predchozi^.levy=list then Predchozi^.levy:=nil
    else Predchozi^.pravy:=nil;
    dispose (list);
  end;

procedure SmazVeVetvi( vrchol, Predchozi:ukVrchol)
var podstrom:ukVrchol;
begin
  if vrchol^.levy <> nil then podstrom:=vrchol^.levy
  else podstrom:=vrchol^.pravy;
  
  if Predchozi^.levy=vrchol then Predchozi^.levy:=podstrom
  else Predchozi^.pravy:=podstrom;
  
  dispose(vrchol);
end

procedure SmazVeStromu(vrchol: ukVrchol)
var podstrom, predchozi:ukVrchol;
begin
podstrom:=vrchol^.levy;
predchozi:=vrchol;
while podstrom^.pravy <> nil do
    begin
    predchozi:=podstrom;
    podstrom:=podstrom^.pravy;
    end;

vrchol^.data:=podstrom^.data;

if podstrom^.levy=nil then SmazList(podstrom, predchozi) else SmazVeVetvi(podstrom, predchozi);

end;
  
procedure Smaz( data:integer, var strom:ukvrchol )
var predchozi, vrchol : ukVrchol;
begin
   predchozi:=nil;
   vrchol := najdiVeStromu(data, strom, predchozi);
   if vrchol <> nil then 
   begin
      if vrchol^.levy = nil and vrchol^.pravy = nil then SmazList(vrchol, predchozi)
      else
	begin
	  if vrchol^.levy <> nil and vrchol^.pravy <> nil then SmazVeStromu
	  else
	    begin
	      SmazVeVetvi(vrchol, predchozi)
	    end;
	end;
      
   end;
end;















































