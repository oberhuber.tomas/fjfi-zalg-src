type pole array[ 0..N ] of integer

procedure primyVyber( var a: pole )

var i,AktNejmensi,k: integer;
begin
  for k:= 1 to N do
    begin
      AktNejmensi := k;
      for i:= k to N do
	begin
	  if a[i] < a[AktNejmensi]
	    then AktNejmensi := i;
	end;
      a[0]:= a[AktNejmensi];
      a[AktNejmensi] := a[k];
      a[k] := a[0];
    end;
end;
