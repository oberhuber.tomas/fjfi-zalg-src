type pole array[ 0..N ] of integer

procedure primyVyber( var a: pole )
var i,AktNejmensi,k: integer;
begin
  for k:= 1 to N do
    begin
      AktNejmensi := k;
      for i:= k to N do
	begin
	  if a[i] < a[AktNejmensi]
	    then AktNejmensi := i;
	end;
      a[0]:= a[AktNejmensi];
      a[AktNejmensi] := a[k];
      a[k] := a[0];
    end;
end;

procedure primeVkladani( var a : pole )
var i: integer;
begin
  for k:= 2 to N do
    begin
      i := k-1;
      a[0] := a[k];
      while a[i] > a[0] do
	begin
	  a[i+1] := a[i];
	  dec(i);
	end
      a[i+1] := a[0];
    end
end;

procedure bubbleSort( var a : pole )
var i, j, leva, prava: integer;
begin
  leva:=1;
  prava:=n-1;
  while prava>leva do begin
    for j=leva to prava do begin
      if a[j] > a[j+1] then begin
      a[0]:=a[j];
      a[j]:=a[j+1];
      a[j+1]:=a[0];
      end;
    end;
    dec(prava);
    for j=prava downto leva do begin
      if a[j] < a[j+1] then begin
      a[0]:=a[j];
      a[j]:=a[j+1];
      a[j+1]:=a[0];
      end; 
    end;
    inc(leva);
  end;
end;

procedure bubbleSort2( var a : pole )
var i, j, zmena, leva, prava: integer;
begin
  leva:=1;
  prava:=n-1;
  while prava>leva do begin
    zmena:=leva;  
    for j=leva to prava do begin
      if a[j] > a[j+1] then begin
      a[0]:=a[j];
      a[j]:=a[j+1];
      a[j+1]:=a[0];
      zmena:=j;
      end;
    end;
    prava:=zmena-1;
    zmena:=prava;
    for j=prava downto leva do begin
      if a[j] < a[j+1] then begin
      a[0]:=a[j];
      a[j]:=a[j+1];
      a[j+1]:=a[0];
      zmena:=j;
      end; 
    end;
    leva:=zmena+1;
  end;
end;


procedure quickSort( var a : pole; left, right : integer )
var leva, prava:integer; 
begin
  leva:=left;
  prava:=right-1;
  
  while leva<>prava do begin
    while a[leva]<a[right] do
      inc(leva);
    while (a[prava]>a[right] and prava>leva) do
      dec(prava);
      
   if prava<>leva then begin
      a[0]:=a[leva];
      a[leva]:=a[prava];
      a[prava]:=a[0];    
   end;
  end;
  
  a[0]:= a[prava];
  a[prava]:=a[right];
  a[right]:=a[0];

  if prava - 1 > left then 
    quickSort( a, left, prava - 1 );
  if prava + 1 < right then
    quickSort( a, prava + 1, right );
end;

  


  
  
    
end;

























