program sachovyKun;

type Sachovnice = object

     public

     {nuluje sachovnici}
     constructor vytvor;

     procedure zapisNaPolicko( i, j, hodnota : integer; );

     function nactiZPolicka( i, j: integer ) : integer;

     {Metoda vraci novy skok sachovym konem z policka o souradnicich i, j.
      Funkce osetruje skoky mimo sachovnici, ktere nevraci.
      Novy skok vraci v promenncyh i1, j1.
      V prvnim volani pozaduje smer = 0 a vraci vzdy skok dalsim moznym smerem.
      Pokud jiz neni kam skocit, vraci false, jinak true. }
     function skokKonem( i,j integer; var smer, i1, j1 : integer ) : boolean;

     private

     sachovnice = array[ 1..8, 1..8 ] of integer;
end;

function pole. skokKonem( i,j integer; var smer, i1, j1 : integer ) : boolean
begin
     while smer < 8 do
     begin
          smer := smer + 1;
          case smer of
          1 :  begin
              i1 := i + 1;
              j1 := j + 2;
          2 : begin
              i1 := i + 2;
              j1 := j + 1;

          if i1 >= 1 and i1 <= 8 and j1 >= 1 and j1 <= 8 then
          begin
               skokKonem := true;
               exit;
          end
     end
     skokKonem := false;
end

function projdiSachovnici( poziceI, poziceJ, krok : integer, var pole:sachovnice ):boolean;
         var smer:integer;
             novapoziceI, novapoziceJ:integer;
begin
   if pole.nactizpolicka(poziceI,poziceJ)<>0 then  {na tomto policku jsme jiz byli, vracime se}
   begin
      projdisachovnici:=false;
      exit;{vyskakujeme z fukce}
   end
   pole.zapisnapolicko(poziceI,poziceJ,krok);      {provadime skok na policko}
   if krok=64 then                                 {nasli jsme reseni}
   begin
      projdisachovnici:=true;
      exit;
   end
   smer:=0;
   while pole.skokKonem(poziceI,poziceJ,smer,novapoziceI,novapoziceJ)= true do {zkousime skoky vsemi moznymi smery}
   begin
      if projdisachovnici(novapoziceI,novapoziceJ,krok+1,pole)= true then
      begin
         projdisachovnici:=true;                                               {tato cesta vede k cili, dal jiz nic hledame}
         exit;
      end
   end
   pole.zapisnapolicko(poziceI,poziceJ,0);                                     {vracime se z daneho policka}
   projdisachovnici:=false;
end.

