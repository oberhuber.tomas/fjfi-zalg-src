program Strom;

type ukVrchol = ^Vrchol;
Vrchol = record
       data : integer;
       levy, pravy : ukVrchol;
end;

type Strom = object
     koren : ukVrchol;

     constructor vytvor;

     procedure vloz( data : integer );

     function hledej( data : integer ) : boolean;

     procedure smazPrvek( data : integer );

     destructor smazStrom();

     {pomocne metody}

     function novyVrchol( data : integer ) : ukVrchol;

     procedure vlozDoStromu( data : integer, korenPodstromu : ukVrchol );

     procedure smazPodstrom( korenPodsttromu : ukVrchol );

     function hledejVPodstromu( data : integer, korenPodstromu : ukVrchol, var predchozi : ukVrchol ) : ukVrchol;

     procedure smazList( list, predchozi : ukVrchol );

     procedure smazVeVetvi( list, predchozi : ukVrchol );

end;

constructor Strom. vytvor
begin
     koren := nil;
end;

function Strom. novyVrchol( data : integer ) : ukVrchol
begin
     new( novyVrchol );
     novyVrchol^. data  := data;
     novyVrchol^. levy  := nil;
     novyVrchol^. pravy := nil;
end;

procedure Strom. vloz( data : integer )
begin
     vlozDoStromu( data, koren );
end;

procedure Strom. vlozDoStromu( data : integer, var korenPodstromu : ukVrchol )
begin
     if korenPodstromu = nil then
        korenPodstromu := novyVrchol( data )
     else
     begin
          if korenPodstromu^. data < data then
             vlozDoStromu(data, korenPodstromu^.levy)
          if korenPodstromu^. data > data then
             vlozDoStromu(data, korenPodstromu^.pravy)
     end;
end;

procedure Strom. smazPodstrom( korenPodstromu : ukVrchol )
begin
     if korenPodstromu^. levy <> nil then
        smazPodstrom( korenPodstromu^. levy );
     if korenPodstromu^. pravy <> nil then
        smazPodstrom( korenPodstromu^. pravy );
     dispose( korenPodstromu );
end;

function Strom. hledej( data : integer ) : boolean
var predchozi : ukPrvek;
begin
     if koren <> nil then
        if hledejVPodstromu( data, koren, predchozi ) = nil then
           hledej := false
        else hledej:= true;
     else
         hledej := false;

end;

function Strom. hledejVPodstromu( data : integer,
                                  korenPodstromu : ukVrchol,
                                  var predchozi : ukVrchol ) : ukVrchol
begin
     if data = korenPodstromu^. data then hledejVPodstromu := korenPodstromu;
     if data < korenPodstromu^.data then
        if korenPodstromu^.levy <> nil then
        begin
             predchozi := korenPodstromu;
             hledejVPodstromu := hledejVPodstromu(data, korenPodstromu^.levy, predchozi )
        end
        else hledejVPodstromu := nil;
     if data> korenPodstromu^.data then
        if korenPodstromu^.pravy <> nil then
        begin
             predchozi := korenPodstromu;
             hledejVPodstromu:=hledejVPodstromu(data, korenPodstromu^.pravy, predchozi )
        end
        else hledejVPodstromu := nil;
end;

procedure Strom. smazList( list, predchozi : ukVrchol )
begin
     {assert( list=predchozi^.levy or list=predchozi^.pravy);}
     {assert( list^.levy = nil and list^.pravy = nil);}
     if list=predchozi^.levy then
        predchozi^.levy := nil
     else
         predchozi^.pravy := nil;
     dispose(list);
end

procedure Strom. smazVeVetvi( list, predchozi : ukVrchol )
var
   nasledujici: ukVrchol;
begin
     {assert( list=predchozi^.levy or list=predchozi^.pravy);}
     {assert( list^.levy <> nil or list^.pravy <> nil);}
     {assert( not (list^.levy <> nil and list^.pravy <> nil)))}
     if list^.levy = nil then
        nasledujici:= list^.pravy
     else
         nasledujici:= list^.levy;
     if list=predchozi^.levy then
        predchozi^.levy := nasledujici
     else
         predchozi^.pravy := nasledujici;
     dispose(list);
end;

procedure Strom. smazPrvek( data : integer )
var vrchol, predchozi, pomocny : ukVrchol
begin
vrchol:= hledejvpodstromu (data, koren, prechozi);
if vrchol<>nil then
   begin
   if (vrchol^.levy = nil) and (vrchol^.pravy = nil) then
      {Mazani listu}
      smazlist(vrchol, predchozi);
   else
       if (vrchol^.levy <> nil) and (vrchol^.pravy <> nil) then
       begin
            {Mazani ve stromu}
       end
       else
       begin
            {Mazani ve vetvi}
            smazvevetvi(vrchol, prechozi);
       end
   end;

end

destructor Strom. smazStrom()
begin
     if koren <> nil then
        smazPodstrom( koren );
end;









