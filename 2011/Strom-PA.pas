program Strom;

type ukVrchol = ^Vrchol;
Vrchol = record
       data : integer;
       levy, pravy : ukVrchol;
end;

type Strom = object
     koren : ukVrchol;

     constructor vytvor;

     procedure vloz( data : integer );

     function hledej( data : integer ) : boolean;

     procedure smazPrvek( data : integer );

     destructor smazStrom();

     {pomocne metody}

     function novyVrchol( data : integer ) : ukVrchol;

     procedure vlozDoStromu( data : integer, korePodstromu : ukVrchol );

     procedure smazPodstrom( korenPodsttromu : ukVrchol );
end;

constructor Strom. vytvor
begin
     koren := nil;
end;

function Strom. novyVrchol( data : integer ) : ukVrchol
begin
     new( novyVrchol );
     novyVrchol^. data  := data;
     novyVrchol^. levy  := nil;
     novyVrchol^. pravy := nil;
end;

procedure Strom. vloz( data : integer )
begin
     vlozDoStromu( data, koren );
end;

procedure Strom. vlozDoStromu( data : integer, var korenPodstromu : ukVrchol )
begin
     if korenPodstromu = nil then
        korenPodstromu := novyVrchol( data )
     else
     begin
          if korenPodstromu^. data < data then
             vlozDoStromu(data, korenPodstromu^.levy)
          if korenPodstromu^. data > data then
             vlozDoStromu(data, korenPodstromu^.pravy)
     end;
end;

procedure Strom. smazPodstrom( korenPodstromu : ukVrchol )
begin
     if korenPodstromu^. levy <> nil then
        smazPodstrom( korenPodstromu^. levy );
     if korenPodstromu^. pravy <> nil then
        smazPodstrom( korenPodstromu^. pravy );
     dispose( korenPodstromu );
end;

destructor Strom. smazStrom()
begin
     if koren <> nil then
        smazPodstrom( koren );
end;









