program Strom;

type ukVrchol = ^Vrchol;
Vrchol = record
       data : integer;
       levy, pravy : ukVrchol;
end;

type Strom = object
     koren : ukVrchol;

     constructor vytvor;

     procedure vloz( data : integer );

     function hledej( data : integer ) : boolean;

     procedure smazPrvek( data : integer );

     destructor smazStrom();

     {pomocne metody}

     function novyVrchol( data : integer ) : ukVrchol;

     procedure vlozDoStromu( data : integer, korenPodstromu : ukVrchol );

     function hledejVPodstromu( data : integer, korenPodstromu : ukVrchol, var predchozi : ukVrchol ) :  ukVrchol;

     procedure smazList( list : ukVrchol, predchozi : ukVrchol );

     procedure smazVeVetvi( vrchol : ukVrchol, predchozi : ukVrchol );

     procedure smazVrchol( vrchol : ukVrchol, predchozi : ukVrchol );
end;

constructor Strom. vytvor()
begin
     koren := nil;
end;

function Strom. novyVrchol( data : integer ) : ukVrchol
begin
     new( novyVrchol );
     novyVrchol^. data  := data;
     novyVrchol^. pravy := nil;
     novyVrchol^. levy  := nil;
end;

procedure Strom. vlozDoStromu( data : integer, korenPodstromu : ukVrchol )
begin
     if data < korenPodstromu^.data then begin
        if korenPodstromu^.levy = nil then
           korenPodstromu^.levy := novyVrchol( data );
        else
            vlozDoStromu( data, koren^.levy );
        end
    end;
    if data > korenPodstromu^.data then begin
        if korenPodstromu^.pravy = nil then
           korenPodstromu^.pravy := novyVrchol( data );
        else
            vlozDoStromu( data, koren^.pravy );
        end
    end;
end;

procedure Strom. vloz( data : integer )
begin
     if koren = nil then
        koren := novyVrchol( data );
     else
         vlozDoStromu( data, koren );
     end;
end;

procedure Strom.smazPodstrom(korenPodstromu : ukVrchol)
begin
     if korenPodstromu^.levy <> nil then
        smazStrom( korenPodstromu^.levy)
     if korenPodstromu^.pravy <> nil then
        smazStrom(korenPodstromu^.pravy)
     dispose(korenPodstromu);
end;

destructor Strom. smazStrom()
begin
     if koren <> nil then
        smazPodstrom(koren)
end;

function Strom. hledejVPodstromu( data : integer, korenPodstromu : ukVrchol, var predchozi : ukVrchol ) :  ukVrchol
begin

     if korenPodstromu<>nil then
     begin

          if data = korenPodstromu^. data then hledejVPodstromu:= korenPodstromu;
          else
          begin
              predchozi := korenPodstromu;
              if data < korenPodstromu^. data then hledejVPodstromu:=hledejVPodstromu (data, KorenPodStromu^.levy, predchozi );
              if data > korenPodStromu^. data then hledejVPodstromu:=hledejVPodstromu (data, KorenPodStromu^.pravy, predchozi );
          end
    else
        hledejVPodstromu := nil;
    end
end

function Strom. hledej( data : integer ) : boolean
var predchozi : ukVrchol;
begin
     if koren = nil then hledej := false;
     else
         if hledejVPodstromu( data, koren, predchozi ) = nil then
            hledej := false;
         else
             hledej := true;
end

procedure Strom. smazList( list : ukVrchol, predchozi : ukVrchol )
begin
     {assert( predchozi <> nil );}
     {assert( predchozi^. levy = list or predchozi^. pravy = list );}

     if predchozi^. levy = list then
     predchozi^. levy = nil
     else predchozi^. pravy = nil;
     dispose( list );
end

procedure Strom. smazVeVetvi( vrchol : ukVrchol, predchozi : ukVrchol )
var nasledujici: UkVrchol
begin
     if vrchol^. pravy = nil then
        nasledujici = vrchol^. levy
     else
         nasledujici= vrchol^. pravy;

     if predchozi^.pravy = vrchol then
        predchozi^. pravy = nasledujici
     else
        predchozi^. levy = nasledujici;
     dispose( vrchol );
end

procedure Strom. smazVrchol( vrchol : ukVrchol, predchozi : ukVrchol )
var pomocny : ukVrchol;
    hledej : boolean;
begin
     pomocny = vrchol.pravy
     predchozi = pomocny
     hledej := true;
     while hledej do
     begin
           predchozi = pomocny
           if pomocny. levy <> nil then
                pomocny = pomocny^.levy
           else
               hledej := false;
           end
     end
     vrchol^.data = pomocny^.data
     if pomocny^.pravy = nil then
        smazList( pomcny, predchozi );
     else
         smazVeVetvi( pomocny, predchozi );
end





