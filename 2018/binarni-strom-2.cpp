#include "binarni-strom.h"


 Strom::Strom()
 {
     koren = NULL;
 }
    
 bool Strom::vloz( double data )
 {
     if(koren == NULL){
         koren = new Uzel;
         koren->data = data;
         koren->levy = NULL;
         koren->pravy = NULL;
     }
     else
         vlozDoPodstromu( koren, data );
 }
    
 bool Strom::vlozDoPodstromu( Uzel* podstrom, double data )
 {
     if( data < podstrom->data )
     {
         if( podstrom->levy == NULL )
         {
            Uzel* novy = new Uzel;
            novy->data = data;
            novy->levy = NULL;
            novy->pravy = NULL;
            podstrom->levy = novy;
            return true;
         }
         return vlozDoPodstromu( podstrom->levy, data );
     }
     if ( data > podstrom->data )
     {
         if( podstrom->pravy == NULL )
         {
            Uzel* novy = new Uzel;
            novy->data = data;
            novy->levy = NULL;
            novy->pravy = NULL;
            podstrom->pravy = novy;
            return true;
         }
         return vlozDoPodstromu( podstrom->pravy, data );
     }
     return false;
 }
 
 bool Strom::najdi( double data )
 {
     if (koren == NULL)
     { return false;  }
     else 
        return najdiVeStromu (koren, data)
 }
    
 bool Strom::najdiVeStromu( Uzel* strom, double data )
 {
     if (strom == NULL)
         return false;
     if ( strom->data == data)
         return true;
     if (strom->data > data)
         return najdiVeStromu (strom->levy, data );
     else 
         return najdiVeStromu (strom->pravy, data);
     
 }
 
 bool Strom::smaz( double data )
 {
     Uzel* predchudce = NULL;
     Uzel* uzel = koren;
     
     while(uzel->data != data)
     {
         predchudce = uzel;
         if (uzel->data < data)
            uzel = uzel -> pravy;
         else         
             uzel = uzel -> levy;
         if (uzel == NULL)
             return false;
     }
     if (uzel -> levy==NULL && uzel -> pravy == NULL)
         smazList(uzel, predchudce);
     else if (uzel -> levy == NULL || uzel -> pravy == NULL)
         smazVeVetvi(uzel, predchudce);
     else 
         smazVeStromu(uzel );
     return true;
 }
 
 void Strom::smazList( Uzel* uzel, Uzel* predchudce )
 {
     if (predchudce->levy == uzel)
         predchudce->levy = NULL;
     else
          predchudce->pravy = NULL;
     delete uzel;    
 }
 
 void Strom::smazVeVetvi( Uzel* uzel, Uzel* predchudce )
 {
     Uzel* pom;
     if(uzel->pravy != NULL)
         pom = uzel->pravy;
     else
         pom = uzel->levy;
     if (predchudce->levy == uzel)
         predchudce->levy = pom;
     else
         predchudce->pravy = pom;
     delete uzel;
 }
 
 void Strom::smazVeStromu( Uzel* uzel )
 {
     Uzel* puvodni = uzel;
     Uzel* predchudce = uzel;
     uzel = uzel->pravy;
     while( uzel->levy != NULL )
     {
         predchudce = uzel;
         uzel = uzel->levy;
     }
     puvodni->data = uzel->data;
     
     if (uzel -> levy==NULL && uzel -> pravy == NULL)
         smazList(uzel, predchudce);
     else if (uzel -> levy == NULL || uzel -> pravy == NULL)
         smazVeVetvi(uzel, predchudce);
     
 }
 
 
 bool Strom::zapis( const char* soubor )
 {
     
 }
    
 bool Strom::nacti( const char* soubor )
 {
     
 }
    
 Strom::~Strom()
 {
     
 }         