
void quicksort( double* a, int prvni, int posledni )
{
    double pivot = a[posledni - 1 ];
    int leva = prvni;
    int prava = posledni - 2;
    while(leva < prava){
        while(a[leva] < pivot && leva < prava){
             leva++;   
        }
        while(a[prava] > pivot && leva < prava){
            prava--;        
        }
        if(leva != prava){
            double b = a[leva];
            a[leva] = a[prava];
            a[prava] = b;
        }
    }
    if( a[ leva ] > pivot ) {
        double c = a[leva];
        a[leva] = a[posledni - 1];
        a[posledni - 1] = c;
        quicksort(a, prvni, leva);
        quicksort(a, leva + 1, posledni);
    }
    else
       quicksort(a, prvni, posledni - 1 );
}

int cifra( int cislo, int pozice ){ ... }

void radixsort( int* a, int N, int R, int delkaZapisu )
{
    int* aux = new int[ N ];
    for (int p=0; p<delkaZapisu; p++) {
        int zarazky[ R ];
        for (int i=0; i<R; i++) {
            zarazky[i] = 0;
        }
        int soucet( 0 );

        for (int i=0; i<N; i++) {
            int c = cifra( a[i], p);
            zarazky[c]++;
            soucet++;
        }
        for (int z=R-1; z>=0; z--) {
            soucet -= zarazky[ z ]
            zarazky[z] = soucet;
        }
        
        for (int i=0; i<N; i++) {
            int c = cifra( a[i], p);
            aux[ zarazky[ c ] ] = a[ i ];
            zarazky[ c ]++;
        }
        for( int i = 0; i < N; i++ )
            a[ i ] = aux[ i ];
    }
    delete[]  aux;
}

void zaradDoHaldy( int* a, int zacatek, int konec )
{
    int i = zacatek;
    while(true)
    {
        int indexMaxima = i;
        int levy = 2*i+1;
        int pravy = 2*i+2;
        if(levy < konec && a[indexMaxima] < a[levy])
        {
            indexMaxima = levy;
        }
        if(pravy < konec && a[indexMaxima] < a[pravy])
        {
            indexMaxima = pravy;
        }
        if(i==indexMaxima)
        {
            break;
        }
        prohod( a[i] , a[indexMaxima] );
        i = indexMaxima;
    }
}

void heapSort( int* a, int N )
{
    int zacatek = (N-1)/2;
    int konec = N;
    
    for (int i=zacatek; i>=0; --i) {
        zaradDoHaldy(a, i, konec);
    }
    
    for (int j=N-1; j<0; --j) {
        prohod(a[0], a[j]);
        zaradDoHaldy(a, 0, j-1);
    }
}




